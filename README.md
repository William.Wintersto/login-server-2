# Part 2A

## What's wrong with the structure? Could any of these have a security impact?
First the code is very unorganized. There are many differnet file types in the project and every file is in the same folder.
There are also files like hello.py that does not contribute to the project and just take up space.
A confusing structure and no unit tests makes it hard for developers to notice potential security risks.

The functions send() and search() use database queries with very poor security.
An SQL injection could be done by for example searching: ';DROP TABLE users; --

Since SQL injections is a problem you might not be able to know who actually sends a message.
The only link between user and message is the username, which a SQL injection could easily modify.

XSS is also a problem since there is nothing stopping ut from sending a message like: <br> ```<span onclick=alert(1)></span>``` <br>

CSRF attacks is also a threat. The code never check the CSRF token which means links can reroute prople to our server. A link like: "localhost:5000/send?sender=bob&message=Hi" will send the message "Hi" to bob. The message can of course be much worse for us. You can put an sql injection in the message and when someone clicks the link, they are the one sending the query. Now it looks like some innocent person hacked the server by clicking the link, and we have no idea who the real attacker is.

## Refactoring
* Deleted hello.py (useless file)
* Deleted nocoffie() and gotcoffie() (useless functions)
* Moved index.html to templates folder
* Made an assets folder and moved favicon.ico and favicon.png into it
* Spaced out and grouped the related functions in app.py to make it more readable



# Part 2B

There is a lot I wanted to improve and implement but unfortunately due to overlapping deadline the project had to be very rushed.
So I will explain a lot about what i would have done in an ideal situation.

## Design considerations

First thing i did was improve the structure like i explained in 2A so i won't repeat that. What i can say is what i did not have time to implement. I allso wanted to split the app.py up in different parts. One file for all the user handeling like logging inn and out, passwords etc. One file for all the visual stuff and redirection between pages, send()/search() ect. And keep the database setup in it's own file. But when trying to do the i got too many problems with imports and files not finding others location that i in the end just thrashed the idea and did the second best which is just group related functions together in app.py to make it a little better. I had another idea last minute about splitting the html file into html, css and js files but had truble and time ran out, so they stay in one file.

My first implementation was the database. Having username and password in plaintext in a dictionary is not very safe. They are now hashed and stored in the database. Users are manually put into the database at the bottom of app.py. I was planning to make a way to register but had no time. The registration feature would have demanded that the new user made a unique username and a strong password which then would be hashed and sent to the database. password_strength_check() is an unused function in the program that was ment to do this check. A strong password and hashing is good to prevent against brute force and rainbow table attacks. I did not have time to figure out how to store session data with flask.

I updated the login function. Now user is redirected to index page if they are allready authenticated. It takes the username and password, checks the database if the user exist, hash the password and compares it to the one stored in the database. Another thing i planned to do was to make a logout button. This is another feature i had no time to implement so the only way is to write "url/logout". It is not very user friendly but it works.

I implemented the messaging system. Messages are stored in the database with (id, sender, reciever, message, time). Originally there was a field to chose who the sender of a message was. This did not make sense to me as other people could potentially write messages in the name of others. So i changed the field to rather chose the recipient. The sender would allways be the logged in user. You can't send an empty message and the recipient has to be an existing user that is not themself.

The plan was to protect agains XXS in user input by not parse html code using something like "innerText" in html but im runnning out of time. I did not have time to implement a feature where only the recipient and sender can see a message. Currently everyone has access to every message via search which is not ideal.

There is no CSRF token to protect the sending of messages currently. This mean we can't control if a POST request has come from somewhere else. Logging in has protection through wtforms, but I was not able to implement it for messages. CSRF protection would ensure tracability and accountability since we can trust that the given sender is the actual sender.

I have used prepared statements on the user imput the goes directly to a SQL query, used in search() and send(). Removing these dangerous symbols prevent agains SQL injections. I also noticed that the app's secret key was a short and insecure string that was allways the same. I replaced this with secrets.token_hex() that generate a random, much safer key. Since it is randomiced, every user will be logged out when the server restarts since their session cookie is invalid.


## Application features

### Logging in
Using the "/login" page you write in your username and password. Password is case sensitive, username is not. After logging in you will be redirected to the "/index.html" page where the message board is.

### Messaging board
Here you can search for messages by content (has to be excact), chose a recipient of your message and write the message. When sending the message in the format it is in the database shows up. Using "Show all" lists all messages stored in the database. You can also see the by searching "*".

### Logging out
As mentioned the plan was to have a logout button, but since there was no time you have to go to the "/logout" page to log out. This will also automatically redirect you to the "/login" page.


## How to test/demo
Writing "flask run" in the terminal will set the application up. After that you can go to "localhost:5000" and be redirected to the login page.

There is no register feature so you have to use one of the two premade accounts with (username, password): (w, 123) and (bob, bananas). These are not secure password and a register function would not let you have weak password like this, but they are simple to make testing more convenient.

When you are logged in you can user the messaging board. Here you can search for messages by content (has to be excact), chose a recipient of your message and write the message. When sending the message in the format it is in the database shows up. Using "Show all" lists all messages stored in the database. You can also see the by searching "*".

As mentioned, if you ever want to switch user you have to go to the "/logout" page to log out. This will also automatically redirect you to the "/login" page.


## Technical details
In addition to the tools used in the original login-server project (like flask_login) I used:
* bcrypt library for hashing and salting passwords
* secrets library for making a secure private key using secrets.token_hex()
* If i had figured out how to use the flask_wtf.csrf.CSRFProtect i would use it to protect messages sent


## Questions

* Threat model – who might attack the application? What can an attacker do? What damage could be done (in terms of confidentiality, integrity, availability)? Are there limits to what an attacker can do? Are there limits to what we can sensibly protect against?

Possible attackers could be owners of other message boards that want to drive people away from my application, criminals looking for the personal information in the database or sent through messages.

XSS could trick people into running scripts that steal their data or they could just extract the entire databse with a SQL injection. Assuming the application was further developed the database might contain a lot more critical data than it does now.

Confidentiality, Integrity and Availability is in danger when extraction, modificaton and removal of data can be done through SQL injection. Ingegritymodification of the data stored is so vulnerable. Accountability and authenticity is also a problem as no CSRF protection makes it possible to fake who the sender is and if XSS is used we have no way of tracing it back to the attacker as the script may be run by a normal user unknowingly.

An attacker might be able to access information outside the messagin board if we consider that a user might use the same password on multiple application. Our pour security measures could result in a security problem on other sites.

With a messaging board this simple there are few features which mean few possible routes of attack. As we add new features we need to constantly reevaluate the security picture, but it does not make sense to implement security features that protect against something that is not yet a possible route of attack. Restiction what messages can be viewed while all messages are still pubic does not make sense.


* What are the main attack vectors for the application?

The main threats are SQL injection, XSS and CSRF attacks. This is done by exploiting the search and message fields. The url may also be targeted.


* What should we do (or what have you done) to protect against attacks?

SQL injections: prepared statements that remove dangerous characters to enter the sql queries

CSRF attack: not implemented, but would have verified the sender of a message as legit and would ensure tracability

XSS: not implemented, but putting user imput into a html element using innerText prevents XSS attacks in most cases.


* What is the access control model?

After the changes i have and planned to implement the acces control model sould been "Discretionary Access Control". Where only a logged in user can view the message board and only sender and recipient can view a private message. Private messages have not been implemented but the "/index.html" page is login required so any user that is not logged in should not be able to view it, but i don't think the authentication process is perfect so it's probably possible to get around this. 


* How can you know that you security is good enough? (traceability)

If I implemented all my ideas, maybe. But it's hard to know what "good enough" security is. Preventing the most common/relevant threats such as SQL injection, XSS and CSRF attacks, while at the same time having features that log user activity is the most important. This enables us to trace the attack back to the attacker and quickly fix the security weakness.
