from http import HTTPStatus
from flask import Flask, abort, request, send_from_directory, make_response, render_template
from werkzeug.datastructures import WWWAuthenticate
import flask
from login_form import LoginForm
from json import dumps, loads
from base64 import b64decode
import sys
import apsw
from apsw import Error
from pygments import highlight
from pygments.lexers import SqlLexer
from pygments.formatters import HtmlFormatter
from pygments.filters import NameHighlightFilter, KeywordCaseFilter
from pygments import token;
from threading import local
from markupsafe import escape
from flask_login import logout_user, current_user
import flask_login
from bcrypt import gensalt, hashpw
import secrets
from datetime import datetime
import bcrypt
import flask_login
from flask_login import login_required, login_user





tls = local()
inject = "'; insert into messages (sender,message) values ('foo', 'bar');select '"
cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')
conn = None

# Set up app
app = Flask(__name__)
# The secret key enables storing encrypted session data in a cookie (make a secure random key for this!)
app.secret_key = secrets.token_hex()

# Add a login manager to the app
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

# We have DATETIME in the database and therfor i need to specify the format 
timestamp_format = '%Y-%m-%d %H:%M:%S'










# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(flask_login.UserMixin):
    pass

# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):

    users_with_this_name = conn.cursor().execute(f'SELECT COUNT(*) FROM users WHERE username=?', (user_id,)).fetchone()[0]
    #If the user does not exist, cancel
    if users_with_this_name == 0:
        return

    # For a real app, we would load the User from a database or something
    user = User()
    user.id = user_id
    return user

# This method is called to get a User object based on a request,
# for example, if using an api key or authentication token rather
# than getting the user name the standard way (from the session cookie)
@login_manager.request_loader
def request_loader(request):
    # Even though this HTTP header is primarily used for *authentication*
    # rather than *authorization*, it's still called "Authorization".
    auth = request.headers.get('Authorization')

    # If there is not Authorization header, do nothing, and the login
    # manager will deal with it (i.e., by redirecting to a login page)
    if not auth:
        return

    (auth_scheme, auth_params) = auth.split(maxsplit=1)
    auth_scheme = auth_scheme.casefold()
    if auth_scheme == 'basic':  # Basic auth has username:password in base64
        (uid,passwd) = b64decode(auth_params.encode(errors='ignore')).decode(errors='ignore').split(':', maxsplit=1)
        print(f'Basic auth: {uid}:{passwd}')
        # Here we check the database instead of the dictionary
        u = conn.cursor().execute(f'SELECT 1 FROM users WHERE username=?', (uid,)).fetchone()
        if u:
            user_password = conn.cursor().execute(f'SELECT 1 FROM users WHERE username=?', (uid,)).fetchone()[0]
            if check_password(user_password, passwd):
                return user_loader(uid)
    elif auth_scheme == 'bearer': # Bearer auth contains an access token;
        # an 'access token' is a unique string that both identifies
        # and authenticates a user, so no username is provided (unless
        # you encode it in the token – see JWT (JSON Web Token), which
        # encodes credentials and (possibly) authorization info)
        print(f'Bearer auth: {auth_params}')
        user_tokens = conn.cursor().execute('SELECT token FROM users').fetchall()
        for token in user_tokens:
            if token == auth_params:
                return user_loader(uid)

    # For other authentication schemes, see
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication

    # If we failed to find a valid Authorized header or valid credentials, fail
    # with "401 Unauthorized" and a list of valid authentication schemes
    # (The presence of the Authorized header probably means we're talking to
    # a program and not a user in a browser, so we should send a proper
    # error message rather than redirect to the login page.)
    # (If an authenticated user doesn't have authorization to view a page,
    # Flask will send a "403 Forbidden" response, so think of
    # "Unauthorized" as "Unauthenticated" and "Forbidden" as "Unauthorized")
    abort(HTTPStatus.UNAUTHORIZED, www_authenticate = WWWAuthenticate('Basic realm=inf226, Bearer'))








def pygmentize(text):
    if not hasattr(tls, 'formatter'):
        tls.formatter = HtmlFormatter(nowrap = True)
    if not hasattr(tls, 'lexer'):
        tls.lexer = SqlLexer()
        tls.lexer.add_filter(NameHighlightFilter(names=['GLOB'], tokentype=token.Keyword))
        tls.lexer.add_filter(NameHighlightFilter(names=['text'], tokentype=token.Name))
        tls.lexer.add_filter(KeywordCaseFilter(case='upper'))
    return f'<span class="highlight">{highlight(text, tls.lexer, tls.formatter)}</span>'

@app.get('/highlight.css')
def highlightStyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    return resp

@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path + "/assets", 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path + "/assets", 'favicon.png', mimetype='image/png')


@app.route('/')
@app.route('/index.html')
@login_required
def index_html():
    return send_from_directory(app.root_path + "/templates", 'index.html', mimetype='text/html')








@app.route('/login', methods=['GET', 'POST'])
def login():

    # If the user is allready logged in we can just redirect to index.html, trying to log in twice can cause problems
    if current_user.is_authenticated:
        return flask.redirect(flask.url_for('index_html'))

    form = LoginForm()
    error = "" # Potential error message

    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username = form.username.data.lower()
        password = form.password.data

        # Checking database if the user exist
        if conn.cursor().execute('SELECT 1 FROM users WHERE username=?', (username,)).fetchone():
            # Knowing the user exist we can get the hashed password
            password_hash = conn.cursor().execute('SELECT password FROM users WHERE username=?', (username,)).fetchone()[0]
            if check_password(password, password_hash):
                user = user_loader(username)
                # automatically sets logged in session cookie
                login_user(user)
                flask.flash('Logged in successfully.')

                return flask.redirect(flask.url_for('index_html'))
        else:
            error = "Wrong username or password"

    return render_template('login.html', form=form, error=error)

# Check if password are matching
def check_password(input_password, hashed_password):
    return bcrypt.checkpw(input_password.encode("utf-8"), hashed_password.encode("utf-8"))

# A strong password has at least 8 characters, 1 capital letter and 1 number
# Not in use since i did not have time to make a register function
# The plan was to have every user make a strong password to register
def password_strength_check(p):
    if len(p) < 8:
        if len(list(filter(lambda c: c.isupper(), p))) < 1: 
            if len(list(filter(lambda c: c.isdigit(), p))) < 1:
                return True
    return False

# Encrypts a password
def hash_password(p):
    salt = bcrypt.gensalt()
    hashed = bcrypt.hashpw(p.encode("utf-8"), salt)
    return hashed

# Planned to make a logout button but did not have time
# Need to use "url/logout"
@app.get('/logout')
@login_required
def logout():
    logout_user()
    return flask.redirect(flask.url_for('login'))

@app.get('/search')
def search():
    query = request.args.get('q') or request.form.get('q') or '*'
    query.replace('\"', '\\"', -1) # Remove dangerous symbols
    stmt = f"SELECT * FROM messages WHERE message GLOB '{query}'"
    result = f"Query: {pygmentize(stmt)}\n"
    try:
        c = conn.execute(stmt)
        rows = c.fetchall()
        result = result + 'Result:\n'
        for row in rows:
            result = f'{result}    {dumps(row)}\n'
        c.close()
        return result
    except Error as e:
        return (f'{result}ERROR: {e}', 500)

@app.route('/send', methods=['POST','GET'])
def send():
    try:

        sender = current_user.id
        recipient = request.args.get('recipient').replace('\"', '\\"', -1) # Remove dangerous symbols
        message = request.args.get('message').replace('\"', '\\"', -1) # Remove dangerous symbols
        timestamp = datetime.now().strftime(timestamp_format)
        
        if not recipient or not message:
            return '{"error": "Missing recipient or message"}'
        if recipient == sender:
            return '{"error": "You can\'t send a message to yourself"}'

        # Can only send messages to existing users
        recipient_exists = conn.cursor().execute('SELECT 1 FROM users WHERE username=?', (recipient,)).fetchone()
        if not recipient_exists:
            return '{"error": "You can only send a message to an excisting user"}'
        else:
            recipient = recipient.lower()

        msg_info = f"('{sender}', '{recipient}', '{message}', '{timestamp}')"
        # Message is stored in the database
        stmt = f"INSERT INTO messages (sender, recipient, message, timestamp) values {msg_info};"
        result = f"Query: {pygmentize(stmt)}\n"
        conn.execute(stmt)
        return f'{result}ok'

    except Error as e:
        return f'{result}ERROR: {e}'

@app.get('/announcements')
def announcements():
    try:
        stmt = f"SELECT author,text FROM announcements;"
        c = conn.execute(stmt)
        anns = []
        for row in c:
            anns.append({'sender':escape(row[0]), 'message':escape(row[1])})
        return {'data':anns}
    except Error as e:
        return {'error': f'{e}'}








try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY, 
        sender TEXT NOT NULL,
        recipient TEXT NOT NULL,
        message TEXT NOT NULL,
        timestamp DATETIME NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS users (
        id integer PRIMARY KEY, 
        username TEXT NOT NULL,
        password TEXT NOT NULL,
        token TEXT);''')

    # Manually puts two users into the database if there are non, did not have time to make register form    
    if c.execute('SELECT COUNT(*) FROM users').fetchone()[0] == 0:
        c.execute(f'INSERT INTO users (username, password, token) VALUES ("w", "{hash_password("123").decode("utf-8")}", "tiktok"), ("bob", "{hash_password("bananas").decode("utf-8")}", "")')

except Error as e:
    print(e)
    sys.exit(1)
